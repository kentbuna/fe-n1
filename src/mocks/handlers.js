import { rest } from 'msw';
import moment from 'moment';

const handler = [
  rest.get('/period', (req, res, ctx) => {
    return res(
      ctx.delay(),
      ctx.status(200),
      ctx.json({
        period: ['last-month', 'this-month', 'yesterday', 'today'],
      }),
    );
  }),
  rest.get('/search', (req, res, ctx) => {
    const requiredParams = ["starttime", "endtime"]
    const searchParams = req.url.searchParams
    const formatDateTime = 'YYYY/MM/DD HH:mm:ss'
    let start = ""
    let end = ""

    try {
      requiredParams.forEach(param => {
        // validate: required
        const requiredPass = searchParams.has(param)
        if (!requiredPass) {
          throw new Error('missing fields.');
        }

        // validate: type
        const value = Number(searchParams.get(param))
        if (isNaN(value)) {
          throw new Error('wrong type.');
        }

        if (param === "starttime") {
          start = moment(value).format(formatDateTime)
        } else if (param === "endtime") {
          end = moment(value).format(formatDateTime)
        }
      })


      return res(
        ctx.delay(),
        ctx.status(200),
        ctx.text(`search from ${start} to ${end}`),
      );
    } catch (e) {
      return res(
        ctx.delay(),
        ctx.status(400),
        ctx.text(e),
      );
    }
  }),
]

export default handler;
