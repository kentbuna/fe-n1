import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Navigation from './comps/Navigation';
import Home from './comps/Home';
import About from './comps/About';
import UserNameContext from './provider/UserNameContext';
import { getUserName } from './utils/store';
import './App.scss';

function App() {
  const [name, setName] = useState(getUserName);
  return (
    <div className="App">
      <BrowserRouter>
        <UserNameContext.Provider value={{ name, setName }}>
          <header className="App-header">
            <Navigation />
          </header>
          <Switch>
            <Route exact path="/" >
              <Home />
            </Route>
            <Route path="/about" >
              <About />
            </Route>
          </Switch>
        </UserNameContext.Provider>
      </BrowserRouter>
    </div>
  );
}

export default App;
