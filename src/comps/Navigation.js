import React from 'react';
import { Link } from 'react-router-dom';
import useUserNameContext from '../hooks/useUserNameContext';

const Navigation = props => {
  const { name } = useUserNameContext();
  return (
    <nav>
      <Link to='/'>Home</Link>
      {name && <Link to='/about'>About</Link>}
    </nav>
  );
}

export default Navigation;
