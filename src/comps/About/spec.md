## 勇敢的挑戰者，歡迎你來到這個神聖的試驗殿堂!

### 在這次的測試中，你必須要完成以下幾個能力測試：

1. 熟悉常用的 modules
    - React-router
    - axios
    - moment
    - immutableJs
    - react-use
2. React Hooks
3. SASS && CSS Flexbox
4. ES6 語法
5. Formik & yup

### 因此，基於以上的條件，題目如下：

1. 你必須增加一個你的測驗答案頁面

    頁面路徑： `/answer`

2. 畫面樣式請參照下圖，撰寫方式請使用 SASS && CSS Flexbox 實作

<layout />

### 需求如下：

-   請實作一個 Form 可以進行時間區間查詢, 包含

    -   **起迄日期選擇** (樣式不限)
    -   **日期期間快速選取** (樣式不限), 請根據 api `GET /period` 動態產生可以選擇項目, 對應時間區間:

    ```
       last-month: 上個月第一天到最後一天
       this-month: 本月第一天到最後一天
       yesterday: 昨天
       today: 今天
    ```

    -   預設查詢狀態為 **今天 00:00:00 ~ 23:59:59**
    -   兩個按鈕分別提供**查詢**及**重置**功能
    -   查詢 api 為 `GET /search?starttime={starttime}&endtime={endtime}` 查詢資料,參數:

        | Parameter | Type                            | Required |
        | --------- | ------------------------------- | -------- |
        | starttime | Milliseconds timestamp (Number) | Yes      |
        | endtime   | Milliseconds timestamp (Number) | Yes      |

    -   重置按鈕點擊後重置為預設查詢狀態

    <demo />

-   實作過程請盡量以 ES6 語法來實作
-   請使用 Function Component, hooks (自己寫或使用 react-use)
